# Ansible Role: scc\_net.network.resolvconf

Write resolv.conf file.

## Dependencies

 - none

## Role Variables

```
resolvconf_nameserver:
  - "2a00:1398::1"
  - "2a00:1398::2"
resolvconf_nameserver_v6_only:
  - "2a00:1398::64:1"
  - "2a00:1398::64:2"

resolvconf_domain: "{{ '.'.join(ansible_fqdn.split('.')[1:]) }}"
resolvconf_search_default:
  - "{{ resolvconf_domain }}"
resolvconf_search: []

resolvconf_sortlist: []
resolvconf_option: []
```
