# Ansible Collection - scc_net.network

Documentation for the collection.

## Requirements
* netdb_api_client (Python Lib)

## Included plugins

### scc_net.network.netdb_interface_config

## Included roles

### scc_net.network.interfaces

[Documentation](roles/interfaces/README.md)

### scc_net.network.resolvconf
