DOCUMENTATION = """
        lookup: netdb_interface_config
        short_description: queries netdb webapi
        description:
            - This lookup can be used to get the information needed to configure the interfaces of a system in KITnet.
        options:
          _terms:
            description: IP-Address
            required: True
          configfile:
            description: config-file path for netdb config
            type: string
            default: '~/.config/netdb_client.ini'
            ini:
              - section: netdb
                key: configfile
            env:
              - name: NETDB_CONFIGFILE
"""
import ipaddress
import os

from ansible import constants as C
from ansible.errors import AnsibleError
from ansible.utils.display import Display
from ansible_collections.scc_net.base.plugins.module_utils.netdb import NetdbLookupModule
from diskcache import Cache

display = Display()


class LookupModule(NetdbLookupModule):

    def _ip_info(self, ip_addr, **kwargs):
        ta = [
            {"idx": "subnet", "name": "nd.ip_subnet.list", "old": {"cidr": str(ip_addr), "cidr_operator": "smallest_cts"}},
            {"idx": "bcd", "name": "nd.bcd.list", "inner_join_ref": {"subnet": "default"}},
            {"idx": "bcd_subnets", "name": "nd.ip_subnet.list", "inner_join_ref": {"bcd": "default"}},
        ]

        display.v(f"Query subnet information for IP {ip_addr}.")
        self._init_netdb()
        query_result = self.api.execute_ta(ta, dict_mode=True)
        display.vvv(f"Got result from netdb: {query_result}")

        result = {
            "v6": {
                "cidr": None,
                "gateway": None,
            },
            "v4": {
                "cidr": None,
                "gateway": None,
            },
            "bcd": query_result["bcd"][0]["name"]
        }
        for subnet in query_result["bcd_subnets"]:
            result[f"v{subnet['type']}"] = {
                "cidr": subnet["cidr"],
                "gateway": subnet["default_gateway"],
            }

        display.vv(f"Got bcd '{result['bcd']}' from netdb.")
        display.vvv(f"Returning: {result}")
        return result

    def run(self, terms, variables=None, **kwargs):
        if len(terms) > 1:
            raise AnsibleError("The Plugin accepts only one parameter (IP-Address).")

        try:
            ip = ipaddress.ip_address(terms[0])
        except ValueError:
            raise AnsibleError(f"{terms[0]} is no valid IP-Address.")

        display.vv(f"Running netdb_interface config for IP {ip}")

        with Cache(os.path.join(C.DEFAULT_LOCAL_TMP, "netdb_cache")) as cache:
            key = (str(ip), kwargs)

            try:
                result = cache[key]
                display.vv(f"Cache hit for {key}")
            except KeyError:
                display.vvv(f"Cache miss, query netdb.")
                result = self._ip_info(str(ip), **kwargs)
                cache[key] = result
        return [result]
