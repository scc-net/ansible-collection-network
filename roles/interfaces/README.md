## scc_net.network.interfaces

This role is derived from the role [ansible-network-interfaces](https://github.com/dresden-weekly/ansible-network-interfaces) by [dresden-weekly](https://github.com/dresden-weekly) and the [modifications](https://github.com/Oefenweb/ansible-network-interfaces/) of the role by [Oefenweb](https://github.com/Oefenweb).

We modified some behaviours to match our needs and preferences. 

Manage network interfaces on Debian-like systems.

#### Default behaviour

The "default" interface is auto-discovered as first (not "lo") interface on the node.

IPs for the interface are resolved from the `interfaces_default_hostfqdn`.

The additional information (gateway and netmask) gets discovered by the `scc_net.network.netdb_interface_config` lookup-plugin.

#### Requirements

* netdb_client_lib

#### Variables

##### General

#### Variables



Variables regarding default interface configuration:

* `interfaces_config_primary`: [default: `True`]: Enables configuration of the "default" interface
* `interfaces_default_device`: [default: `ansible_interfaces | reject('match', '^(lo|docker0|br-.*)$') | sort | first`]: Interface to be configured as default, the default value is likely to be correct but won't be for sure
* `interfaces_default_hostfqdn`: [default: `inventory_hostname`]: FQDN used to resolve the hosts IP-Addresses
* `interfaces_primary_ipv6_address`: [default: `query('community.general.dig', interfaces_default_hostfqdn, 'qtype=AAAA') | first`]: IPv6 to be configured of the default interface
* `interfaces_primary_ipv4_address`: [default: `query('community.general.dig', interfaces_default_hostfqdn, 'qtype=A') | first`]: IPv4 to be configured of the default interface
* `interfaces_primary_network_info`: [default: `lookup('scc_net.network.netdb_interface_config', interfaces_default_ip6)`]: Dict containing the network information from netdoc. For the content, see below.
* `interfaces_primary_ipv6_netmask`: [default: `interfaces_primary_network_info.v6.cidr | ansible.utils.ipaddr('prefix'))`]
* `interfaces_primary_ipv6_gateway`: [default: `interfaces_primary_network_info.v6.gateway`]
* `interfaces_primary_ipv4_netmask`: [default: `interfaces_primary_network_info.v4.cidr | ansible.utils.ipaddr('prefix'))`]
* `interfaces_primary_ipv4_gateway`: [default: `interfaces_primary_network_info.v4.gateway`]

Variables content structure:
```yaml
interfaces_default_network_info:
  v6:
    gateway: 2001:DB8::1
    cidr: 2001:DB8::/64
  v4:
    gateway: 192.0.2.1
    cidr: 192.0.2.0/24
```
The `v6` or `v4` part can be omited if the `interfaces_default_hostfqdn` does not resolve to an `AAAA` respective `A` record.

* `interfaces_config`: [default: `{}`]: Network interfaces declarations
* `interfaces_config.{device}.{n}`: [default: `[]`]: Network configurations for one device
* `interfaces_config.{device}.{n}.auto`: [default: `true`]: Enable on boot (if one entry in the list has auto = true, it will be set)
* `interfaces_config.{device}.{n}.family`: [default: `inet6`]: Network type, eg. inet | inet6
* `interfaces_config.{device}.{n}.method`: [default: `auto`]: Method of the interface, eg. dhcp | static

* `interfaces_config.{device}.{n}.description`: [optional]: Description
* `interfaces_config.{device}.{n}.address`: [optional]: Address
* `interfaces_config.{device}.{n}.network`: [optional]: Network address
* `interfaces_config.{device}.{n}.netmask`: [optional]: Netmask
* `interfaces_config.{device}.{n}.broadcast`: [optional]: Broadcast address
* `interfaces_config.{device}.{n}.gateway`: [optional]: Default gateway
* `interfaces_config.{device}.{n}.nameservers`: [optional]: List of nameservers for this interface
* `interfaces_config.{device}.{n}.dns_search`: [optional]: Search list for host-name lookup
* `interfaces_config.{device}.{n}.mtu`: [optional]: MTU of the interface

* `interfaces_config.{device}.{n}.subnets`: [optional]: List of additional subnets, eg. ['192.168.123.0/24', '192.168.124.11/32']

##### Bridge

* `interfaces_config.{device}.{n}.bridge`: [optional, default: `{}`]: Bridge declarations
* `interfaces_config.{device}.{n}.bridge.ports`: [optional]: Bridge ports
* `interfaces_config.{device}.{n}.bridge.stp`: [optional]: Turn spanning tree protocol on/off
* `interfaces_config.{device}.{n}.bridge.fd`: [optional]: Bridge forward delay
* `interfaces_config.{device}.{n}.bridge.maxwait`: [optional]: Maximum time to wait for the bridge ports to get to the forwarding status
* `interfaces_config.{device}.{n}.bridge.waitport`: [optional]: Maximum time to wait for the specified ports to become available

##### VLAN Config

* `interfaces_config.{device}.{n}.vlan`: [optional, default: `{}`]: VLAN Configuration
* `interfaces_config.{device}.{n}.vlan.id`: [optional, default: `{}`]: VLAN ID
* `interfaces_config.{device}.{n}.vlan.raw-device`: [optional, default: `{}`]: VLAN Raw Device

##### Inline hook scripts

* `interfaces_config.{device}.{n}.pre-up`: [optional, default: `[]`]: List of pre-up script lines
* `interfaces_config.{device}.{n}.up`: [optional, default: `[]`]: List of up script lines
* `interfaces_config.{device}.{n}.down`: [optional, default: `[]`]: List of down script lines
* `interfaces_config.{device}.{n}.post-down`: [optional, default: `[]`]: List of post-down script lines


#### Example(s)

##### DigitalOcean droplet with private networking enabled

```yaml
---
- hosts: all
  roles:
    - network-interfaces
  vars:
    interfaces_config:
      ens192:
        - auto: true
          family: inet
          method: static
          address: 188.166.9.28
          netmask: 255.255.0.0
          gateway: 188.166.0.1
          mtu: 1500
          nameservers:
            - 8.8.8.8
            - 8.8.4.4
          up:
            - 'ip addr add 10.18.0.8/16 dev ens192'
      ens224:
        - auto: true
          family: inet
          method: static
          address: 10.133.136.172
          netmask: 255.255.0.0
```

#### License

MIT

#### Author Information

* Andreas Reischuck
* Mark van Driel
* Mischa ter Smitten
* Dominik Rimpf <dominik.rimpf@kit.edu>
